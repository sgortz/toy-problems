# Toy Problems

Welcome to Toy Problems. This repository will be updated daily with a new code challenge.

Feel free to use Google to aid you in solving the coding challenges!

## Getting started

If you haven't already, fork the repository on GitHub and clone your newly created repo down to your computer using the `git clone` command. 

## Updating the Repository

Every night, when a new toy problem is added, you'll need to sync your version of the repo. Git won't automatically pull in upstream changes for you; it trusts that you'll pull them in as needed. Do so by giving Git a reference to this version of the repo:

```bash
git remote add upstream https://github.com/sgortz/toy-problems.git
```
(You'll need to do the step above only once.)

After you've done that, updating your repo is as simple as running the following:

```bash
git checkout main
git pull upstream main 
```

This will check out your branch and tell git to grab any changes made to the main repository and merge them into your branch.

## TDD, writing your own test

Make sure to write test to your code.
