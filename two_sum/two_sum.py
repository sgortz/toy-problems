# Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
# You may assume that each input would have exactly one solution, and you may not use the same element twice.
# You can return the answer in any order.


# Function definition
def twoSum(nums, target):
    pass



# Unit Tests
print('Should return [0,1]: ', twoSum([2,7,11,15], 9) == [0,1])
print('Should return [1,2]: ', twoSum([3,2,4], 6) == [1,2])
print('Should return [0,1]: ', twoSum([3,3], 6) == [0,1])
